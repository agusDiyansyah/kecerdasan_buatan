<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MX_Controller {

	public function __construct () {
		parent::__construct ();
		
		$this->output->set_title("Administrator");
		$this->output->set_template("admin/default");
	}
}