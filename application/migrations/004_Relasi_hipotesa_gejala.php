<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Relasi_hipotesa_gejala extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS relasi_hipotesa_gejala;
		");
		$this->db->query("
			CREATE TABLE `relasi_hipotesa_gejala` (
				`id_hipotesa` int(11),
				`id_gejala` int(11)
			);
		");
	}

	public function down () {}
	
}