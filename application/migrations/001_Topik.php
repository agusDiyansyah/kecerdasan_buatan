<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Topik extends CI_Migration {
	public function up () {
		$this->db->query("
			DROP TABLE IF EXISTS topik;
		");
		$this->db->query("
			CREATE TABLE `topik` (
				`id_topik` int(20) unsigned NOT NULL AUTO_INCREMENT,
				`nama_topik` varchar(255),
				PRIMARY KEY (`id_topik`)
			);
		");
	}

	public function down () {}
	
}