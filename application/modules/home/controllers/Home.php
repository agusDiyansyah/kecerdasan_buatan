<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    protected $module = "home";

    public function index() {
        $this->output->script_foot("$this->module/home/index.js");

        $this->db->query("DROP TABLE IF EXISTS g_temp;");
        $this->db->query("
            CREATE TEMPORARY TABLE g_temp(
                SELECT
                    t.id_topik,
                    COUNT(t.id_topik) jumlah
                FROM
                    topik t
                JOIN gejala g ON t.id_topik = g.id_topik
                GROUP BY
                    t.id_topik
            );        
        ");
        
        $this->db->query("DROP TABLE IF EXISTS h_temp;");
        $this->db->query("
            CREATE TEMPORARY TABLE h_temp(
                SELECT
                    id_topik,
                    COUNT(id_topik) jumlah
                FROM
                    hipotesa
                GROUP BY
                    id_topik
            );        
        ");
        $topik = $this->db
            ->select("
                t.*,
                IF(h.jumlah IS NULL, 0, h.jumlah) h_jumlah,
                IF(g.jumlah IS NULL, 0, g.jumlah) g_jumlah
            ")
            ->from("topik t")
            ->join("h_temp h", "t.id_topik = h.id_topik", "left")
            ->join("g_temp g", "t.id_topik = g.id_topik", "left")
            ->order_By("t.id_topik", "desc")
            ->get();
        $data = array(
            "topik" => $topik
        );

        $this->load->view("$this->module/home/index.php", $data);
    }

    public function proses () {
        $this->output->unset_template();

        $id_topik = $this->input->post("id_topik");
        $nama_topik = $this->input->post("nama_topik");
        $stat = false;

        if (!empty($id_topik)) {
            $proses = $this->db
                ->where("id_topik", $id_topik)
                ->update("topik", array(
                    "nama_topik" => $nama_topik
                ));
        } else {
            $proses = $this->db->insert("topik", array(
                "nama_topik" => $nama_topik
            ));
        }
        
        if ($proses) {
            $stat = true;
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }

    public function hapus() {
        $this->output->unset_template();

        $id_topik = $this->input->post("id_topik");
        $stat = false;

        if (!empty($id_topik)) {
            $proses = $this->db
                ->where("id_topik", $id_topik)
                ->delete("topik");
        } else {
            show_404();
        }

        if ($proses) {
            $stat = true;
            $sql = $this->db
                ->select("id_topik, id_hipotesa")
                ->where("id_topik", $id_topik)
                ->get("hipotesa");
            // hapus gejala
            foreach ($sql->result() as $h) {
                $this->db
                    ->where("id_hipotesa", $h->id_hipotesa)
                    ->delete("gejala");
            }
            // hapus hipotesa
            $this->db
                ->where("id_topik", $id_topik)
                ->delete("hipotesa");
        }

        echo json_encode(array(
            "stat" => $stat
        ));
    }
}

/* End of file Home.php */
