$(document).ready(function () {
    $('.btn-tambah').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-edit').on('click', function (e) {
        e.preventDefault();
        form($(this));
    });

    $('.btn-hapus').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var id_hipotesa = ($(this).data('id_hipotesa')) ? $(this).data('id_hipotesa') : "";

        m.find('.modal-title').html('HAPUS TOPIK');
        m.find('.modal-body').html("ANDA YAKIN AKAN MENGHAPUS DATA INI");
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/hipotesa/hapus") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id_hipotesa: id_hipotesa,
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });
});

function form (obj) {
    var id_topik = (obj.data('id_topik')) ? obj.data('id_topik') : "";
    var id_hipotesa = (obj.data('id_hipotesa')) ? obj.data('id_hipotesa') : "";

    $.ajax({
        url: '<?php echo base_url("home/hipotesa/get_data") ?>',
        type: 'post',
        dataType: 'json',
        data: {
            id_hipotesa: id_hipotesa,
        },
        success: function (json) {
            var m = $('.modal');
            var form = '';
            var aksi_name = (id_hipotesa != '') ? 'UBAH' : 'TAMBAH';
            
            form += '<form class="form-hipotesa">';
            form += '   <div class="row">';
            form += '       <div class="col-md-12">';
            form += '           <div class="form-group">';
            form += '               <label for="">HIPOTESA</label>';
            form += '               <input type="text" value="' + json.data.nama_hipotesa + '" name="nama_hipotesa" class="nama_hipotesa form-control">';
            form += '           </div>';
            form += '       </div>';
            form += '       <div class="col-md-12">';
            form += '           <div class="form-group">';
            form += '               <label for="">SOLUSI</label>';
            form += '               <textarea class="solusi form-control" name="solusi">' + json.data.solusi + '</textarea>';
            form += '           </div>';
            form += '       </div>';
            form += '   </div>';
            form += '   <button class="hidden" type="submit"></button>';
            form += '</form>';
    
            m.find('.modal-title').html(aksi_name + ' HIPOTESA');
            m.find('.modal-body').html(form);
            m.modal();

            m.find('.btn-proses').on('click', function (e) {
                e.preventDefault();
                m.find('.form-hipotesa').submit();
            });
    
            m.find('.form-hipotesa').off().submit(function (e){
                e.preventDefault();
                $.ajax({
                    url: '<?php echo base_url("home/hipotesa/proses") ?>',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id_topik: id_topik,
                        id_hipotesa: id_hipotesa,
                        nama_hipotesa: m.find('.nama_hipotesa').val(),
                        solusi: m.find('.solusi').val(),
                    },
                    success: function (json) {
                        if (json.stat) {
                            location.reload();
                        }
                    }
                });
            });
        }
    });
}