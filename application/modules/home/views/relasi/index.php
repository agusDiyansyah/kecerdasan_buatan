<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border" style="padding: 15px;">
                <h3 class="box-title" style="float: left; margin-top: 5px; margin-bottom: 8px">
                    RELASI / <a href="<?= base_url("home") ?>" class="text-success"><?php echo $nama_topik ?></a>
                </h3>
                <a href="" style="float: right" class="btn btn-sm btn-default btn-tambah" data-id_topik="<?php echo $id_topik ?>">
                    <i class="fa fa-plus"></i> &nbsp
                    TAMBAH DATA
                </a>
            </div>

            <div class="box-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th style="width: 2%">#</th>
                                <th style="width: 10%">HIPOTESA</th>
                                <th style="width: 10%">AKSI</th>
                                <th>RELASI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($hipotesa->num_rows() > 0) {
                                $i = 0;
                                foreach ($hipotesa->result() as $data) {
                                    $i++;
                                    $gejala_ln = base_url("home/gejala/$data->id_hipotesa");
                                    
                                    $gejala = "";
                                    if (!empty($relasi[$data->id_hipotesa])) {
                                        foreach ($relasi[$data->id_hipotesa] as $val) {
                                            $gejala .= "$val <br />";
                                        }
                                    }

                                    echo "
                                        <tr>
                                            <td>$i</td>
                                            <td>$data->nama_hipotesa</td>
                                            <td>
                                                <a href=''
                                                    data-id_topik='$data->id_topik'
                                                    data-id_hipotesa='$data->id_hipotesa'
                                                    data-nama_hipotesa='". ( strtoupper($data->nama_hipotesa) ) ."'

                                                    class='btn btn-sm btn-primary btn-relasi'>
                                                    <i class='fa fa-chain'></i> &nbsp
                                                    TAMBAH RELASI
                                                </a>
                                            </td>
                                            <td>$gejala</td>
                                        </tr>
                                    ";
                                }
                            } else {
                                echo "
                                    <tr>
                                        <td colspan='3'>Data tidak ditemukan</td>
                                    </tr>
                                ";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="box-footer" style="padding: 15px;">
                &nbsp
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">KEMBALI</button>
        <button type="button" class="btn btn-primary btn-proses">PROSES</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->