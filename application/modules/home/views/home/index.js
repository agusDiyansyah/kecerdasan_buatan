$(document).ready(function () {
    $('.btn-tambah').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var form = '';
        var nama_topik = ($(this).data('nama_topik')) ? $(this).data('nama_topik') : "";
        var id_topik = ($(this).data('id_topik')) ? $(this).data('id_topik') : "";

        form += '<div class="row">';
        form += '    <div class="col-md-12">';
        form += '        <div class="form-group">';
        form += '            <label for="">NAMA TOPIK</label>';
        form += '            <input type="text" value="' + nama_topik + '" name="nama_topik" class="nama_topik form-control">';
        form += '            <input type="text" value="'+ id_topik +'" name="id_topik" class="id_topik form-control hide">';
        form += '        </div>';
        form += '    </div>';
        form += '</div>';

        m.find('.modal-title').html('TAMBAH TOPIK');
        m.find('.modal-body').html(form);
        m.modal();

        m.find('.btn-proses').off().on('click', function (e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/proses") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    nama_topik: m.find('.nama_topik').val(),
                    id_topik: m.find('.id_topik') .val(),
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });

    $('.btn-edit').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var form = '';
        var nama_topik = ($(this).data('nama_topik')) ? $(this).data('nama_topik') : "";
        var id_topik = ($(this).data('id_topik')) ? $(this).data('id_topik') : "";

        form += '<div class="row">';
        form += '    <div class="col-md-12">';
        form += '        <div class="form-group">';
        form += '            <label for="">NAMA TOPIK</label>';
        form += '            <input type="text" value="' + nama_topik + '" name="nama_topik" class="nama_topik form-control">';
        form += '            <input type="text" value="' + id_topik + '" name="id_topik" class="id_topik form-control hide">';
        form += '        </div>';
        form += '    </div>';
        form += '</div>';

        m.find('.modal-title').html('UBAH TOPIK');
        m.find('.modal-body').html(form);
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/proses") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    nama_topik: m.find('.nama_topik').val(),
                    id_topik: m.find('.id_topik').val(),
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });

    $('.btn-hapus').on('click', function (e) {
        e.preventDefault();
        var m = $('.modal');
        var id_topik = ($(this).data('id_topik')) ? $(this).data('id_topik') : "";

        m.find('.modal-title').html('HAPUS TOPIK');
        m.find('.modal-body').html("ANDA YAKIN AKAN MENGHAPUS DATA INI");
        m.modal();

        m.find('.btn-proses').off().on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url("home/hapus") ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    id_topik: id_topik,
                },
                success: function (json) {
                    if (json.stat) {
                        location.reload();
                    }
                }
            });
        });
    });
});