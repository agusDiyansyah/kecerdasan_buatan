<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border" style="padding: 15px;">
                <h3 class="box-title" style="float: left; margin-top: 5px; margin-bottom: 8px">TOPIK</h3>
                <a href="" style="float: right" class="btn btn-sm btn-default btn-tambah">
                    <i class="fa fa-plus"></i> &nbsp
                    TAMBAH DATA
                </a>
            </div>
            
            <div class="box-body" style="padding: 15px;">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th style="width: 2%">#</th>
                                <th style="width: 10%">AKSI</th>
                                <th>NAMA TOPIK</th>
                                <th></th>
                                <th style="width: 2%;">DIAGNOSA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($topik->num_rows() > 0) {
                                $i = 0;
                                foreach ($topik->result() as $data) {
                                    $i++;
                                    $hipotesa_ln = base_url("home/hipotesa/$data->id_topik");
                                    $gejala_ln = base_url("home/gejala/$data->id_topik");
                                    $relasi_ln = base_url("home/relasi/$data->id_topik");
                                    $diagnosa_ln = base_url("home/diagnosa/$data->id_topik");
                                    echo "
                                        <tr>
                                            <td>$i</td>
                                            <td>
                                                <a href='' data-nama_topik='$data->nama_topik' data-id_topik='$data->id_topik' class='btn btn-sm btn-info btn-edit'>
                                                    <i class='fa fa-edit'></i>
                                                </a>
                                                <a href='' data-id_topik='$data->id_topik' class='btn btn-sm btn-danger btn-hapus'>
                                                    <i class='fa fa-times'></i>
                                                </a>
                                            </td>
                                            <td>$data->nama_topik</td>
                                            <td>
                                                <div class='btn-group' role='group'>
                                                    <a href='$gejala_ln' class='btn btn-danger'>GEJALA</a>
                                                    <a href='#' class='btn btn-default'>
                                                        <b>$data->g_jumlah</b>
                                                    </a>
                                                </div>

                                                <div class='btn-group' role='group'>
                                                    <a href='$hipotesa_ln' class='btn btn-primary'>HIPOTESA</a>
                                                    <a href='#' class='btn btn-default'>
                                                        <b>$data->h_jumlah</b>
                                                    </a>
                                                </div>

                                                <a href='$relasi_ln' class='btn btn-success'>
                                                    <i class='fa fa-chain'></i> &nbsp
                                                    RELASI
                                                </a>
                                            </td>
                                            <td style='text-align: center'>
                                                <a href='$diagnosa_ln' class='btn btn-sm btn-primary'>
                                                    <i class='fa fa-check'></i>
                                                </a>
                                            </td>
                                        </tr>
                                    ";
                                }
                            } else {
                                echo "
                                    <tr>
                                        <td colspan='5'>Data tidak ditemukan</td>
                                    </tr>
                                ";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="box-footer" style="padding: 15px;">
                &nbsp
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">KEMBALI</button>
        <button type="button" class="btn btn-primary btn-proses">PROSES</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->