<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border" style="padding: 15px;">
                <h3 class="box-title" style="float: left; margin-top: 5px; margin-bottom: 8px"><?php echo $nama_topik ?></h3>
                <a href="" style="float: right" class="btn btn-sm btn-default btn-diagnosa">
                    <i class="fa fa-search"></i> &nbsp
                    DIAGNOSA
                </a>
            </div>
            
            <div class="box-body" style="padding: 15px;">
                <div class="row">
                    <div class="col-md-6">
                        <form action="<?php echo base_url("home/gejala/$id_topik") ?>" method="post" class="form-diagnosa">
                            <input type="hidden" value="<?= $id_topik ?>" name="id_topik">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="2%">NO</th>
                                            <th>GEJALA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0;
                                        foreach ($gejala->result() as $data) {
                                            $no++;
                                            echo "
                                            <tr class='aksi-relasi' style='cursor: pointer'>
                                                <td>$no</td>
                                                <td>
                                                    <input value='$data->id_gejala' type='checkbox' name='id_gejala[]' class='id_gejala hidden'>
                                                    $data->nama_gejala
                                                </td>
                                            </tr>
                                            ";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                    <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>TINGKAT PERSENTASE</th>
                                    <th>NILAI KEMUNGKINAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0 - 50 %</td>
                                    <td>Sedikit Kemungkinan atau Kemungkinan Kecil</td>
                                </tr>
                                <tr>
                                    <td>51 - 79 %</td>
                                    <td>Kemungkinan</td>
                                </tr>
                                <tr>
                                    <td>80 - 99 %</td>
                                    <td>Kemungkinan Besar</td>
                                </tr>
                                <tr>
                                    <td>100 %</td>
                                    <td>Sangat Yakin</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <tr>
                                <td style="font-size: 18px" class="kemungkinan">HASIL DIAGNOSA</td>
                            </tr>
                            <tr>
                                <td style="font-size: 18px">
                                    <div class="solusi">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>DIAGNOSA</th>
                                                    <th>SOLUSI</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="table table-striped table-bordered lainnya">
                                        <thead>
                                            <tr>
                                                <th>DIAGNOSA LAINNYA</th>
                                                <th>BOBOT KEPERCAYAAN</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="box-footer" style="padding: 15px;">
                &nbsp
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">KEMBALI</button>
        <button type="button" class="btn btn-primary btn-proses">PROSES</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->